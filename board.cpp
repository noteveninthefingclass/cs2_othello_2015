#include "board.h"

using namespace std;

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;

    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * number of corners
 */
double Board::cornerScore(Side side){
    return get(side,0,0) + get(side,0,7) + get(side,7,0) + get(side,7,7);
}

double Board::cSquares(Side side){
    return get(side,0,1) + get(side,1,0) + get(side,0,6) + get(side,1,7) + \
    get(side,6,0) + get(side,7,1) + get(side,6,7) + get(side,7,6);
}

double Board::xSquares(Side side){
    return get(side,1,1) + get(side,6,1) + get(side,1,6) + get(side,6,6);
}

double Board::staticHeuristic(Side side){
    int score = 0;
    //corners
    score += 4 * (get(side, 0, 0) + get(side, 0, 7) + get(side,7,0) + get(side,7,7));
    
    //x-square diagonal, doesn't apply if corner is owned
    score += -4 * (!get(side,0,0) * get(side,1,1) + !get(side,0,7) * get(side, 1,6) + !get(side,7,0) * get(side,6,1) + !get(side,7,7) * get(side,6,6));
    
    //x-square horizontal/vertical to corner, doesn't apply if corner is owned
    score += -3 * (!get(side,0,0) * (get(side,1,0) + get(side,0,1)) + \
                   !get(side,0,7) * (get(side,1,7) + get(side,0,6)) + \
                   !get(side,7,0) * (get(side,6,0) + get(side,1,7)) + \
                   !get(side,7,7) * (get(side,6,7) + get(side,7,6)));
    
    //edges
    score += 2 * (get(side,0,2) + get(side,0,3) + get(side,0,4) + get(side,0,5) + \
                  get(side,2,0) + get(side,3,0) + get(side,4,0) + get(side,5,0) + \
                  get(side,7,2) + get(side,7,3) + get(side,7,4) + get(side,7,5) + \
                  get(side,2,7) + get(side,3,7) + get(side,4,7) + get(side,5,7));
    //next to edge squares, doesn't apply if 3 edges next to it are occupied
    if(!(occupied(2,0) * occupied(3,0) * occupied(4,0) * occupied(5,0))){
        score += -1 *(get(side,2,1) + get(side,3,1) + get(side,4,1) + get(side,5,1));
    }
    if(!(occupied(2,7) * occupied(3,7) * occupied(4,7) * occupied(5,7))){
        score += -1 * (get(side,2,6) + get(side,3,6) + get(side,4,6) + get(side,5,6));
    }
    if(!(occupied(0,2) * occupied(0,3) * occupied(0,4) * occupied(0,5))){
        score += -1 * (  get(side,1,2) + get(side,1,3) + get(side,1,4) + get(side,1,5));
    }
    if(!(occupied(7,2) * occupied(7,3) * occupied(7,4) * occupied(7,5))){
        score += -1 * (get(side,6,2) + get(side,6,3) + get(side,6,4) + get(side,6,5));
    }
 /*   score += -1 * (get(side,2,1) + get(side,3,1) + get(side,4,1) + get(side,5,1) + \
                   get(side,2,6) + get(side,3,6) + get(side,4,6) + get(side,5,6) + \
                   get(side,1,2) + get(side,1,3) + get(side,1,4) + get(side,1,5) + \
                   get(side,6,2) + get(side,6,3) + get(side,6,4) + get(side,6,5));
  */
    score += (get(side,2,2) + get(side,5,5) + get(side,2,5) + get(side,5,2));
    score += (get(side,3,3) + get(side,3,4) + get(side,4,3) + get(side,4,4));

    return score;
    
}

/*
 * Calculates the potentialmobility of the current board
 *
 * @param side the side who's potential mobility you wish to calculate
 */
double Board::potentialMobility(Side side){
    if(side == BLACK){
        side = WHITE;
    }else{
        side = BLACK;
    }
    double mobility = 0;
    for(int x = 0; x < 8; x++){
        for(int y = 0; y < 8; y++){
            if(!occupied(x,y)){
                bool found = false;
                for(int checkX = x - 1; checkX <= x + 1; checkX++){
                    for(int checkY = y - 1; checkY <= y + 1; checkY++){
                        if(checkX >= 0 && checkX <= 7 && checkY >= 0 && checkY <=7){
                            if(get(side,checkX, checkY)){
                                found = true;
                            }
                        }
                    }
                }
                if(found){
                    mobility++;
                }
            }
            
        }
    }
    return mobility;
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Returns a vector of all possible moves for a side
 */
vector<Move *> Board::possibleMoves(Side side){
    /*
     * TODO: optimize a lot...
     * currently iterates through squares, promising ones first
     */
    vector<Move *> moves;
    //check corners first
    vector<Move *> viableMoves;
    for(int x = 0; x < 8; x+=7){
        for(int y = 0; y < 8; y+=7){
            Move * curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    //check non-x square edges, horizontals
    for(int x = 2; x < 6; x++){
        for(int y = 0; y < 8; y+=7){
            Move * curMove = new Move(x,y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    //check non-x square edges, verticals
    for(int x = 0; x < 8; x+=7){
        for(int y = 2; y < 6; y++){
            Move *curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    //check centers
    for(int x = 2; x < 6; x++){
        for(int y = 2; y < 6; y++){
            Move *curMove = new Move(x, y); //check if this is correct, might memoryleak
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        
        }
    }
    
    //check next to edge squares
    //horizontals
    for(int x = 2; x < 6; x++){
        for(int y = 1; y < 7; y+=5){
            Move * curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    //verticals
    for(int x = 1; x < 7; x+=5){
        for(int y = 2; y < 6; y++){
            Move *curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    
    //check x-squares
    for(int x = 0; x < 8; x+=7){ //checks (0, 1), (7, 1), (0, 6), (7, 6)
        for(int y = 1; y < 7; y+=5){
            Move *curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    for(int x = 1; x < 7; x+=5){
        for(int y = 0; y < 8; y+=7){ //checks (1, 0), (6, 0), (1, 7), (6, 7)
            Move * curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
        for(int y = 1; y < 7; y+=5){ //checks (1, 1), (1, 6), (6, 1), (6, 6)
            Move *curMove = new Move(x, y);
            if(checkMove(curMove, side)){
                moves.push_back(curMove);
            }else{
                delete curMove;
            }
        }
    }
    return moves;
}
/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
