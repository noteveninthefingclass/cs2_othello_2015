 Vincent: Implementation of ProbCut
Tim: Implementation of heuristic function

Improvements
Search Algorithm
We began with an alphabeta implementation. To increase speed, we implemented a sorted search, with moves we thought were better searched first in order to increase pruning. We then implemented ProbCut, which is an extension of alphabeta that prunes probably bad moves. 

Heuristic
We began with a static weight on each square on the board. The next iteration changed these weights dynamically based on its surrounding squares. The last iteration included the number of available moves for the player and opponent into the heuristic, aiming to minimize the opponent's moves. 