#include "player.h"
#include <limits>
#include <stdio.h>
#include <cmath>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    //set player sides
    playerSide = side;
    if(side == BLACK){
		oppSide = WHITE;
    }
	else{
		oppSide = BLACK;
    }
   
    //change this line to select the algorithm we use
    algorithm = ALPHABETA;
    if(testingMinimax){
		algorithm = MINIMAX;
    }
	
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    board = new Board();
}

/*
 * Constructor for the player with a preinitialized board setup
 */
Player::Player(Board *setupBoard, Side side){
    testingMinimax = false;
    playerSide = side;
    oppSide = otherSide(side);
    board = setupBoard->copy();
}

/*
 * Destructor for the player.
 */
Player::~Player() {

}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /*
     * TODO: implement time tracking, currently just runs until done
     */
	
    
    //add opponent's move to our board
    board->doMove(opponentsMove, oppSide);
    Move *myMove;
    if(testingMinimax){
        myMove = minimax(msLeft, 2);
    }
    //myMove = alphabeta(msLeft, 6);
	myMove = probCut(msLeft, 1, -0.01, .6, 1.5, 4, 2, 6);
    //   myMove = heuristicMove();

    
    board->doMove(myMove, playerSide);
    return myMove;
    
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
}

Move *Player::minimax(int msLeft, int depth){
    if(testingMinimax){
		depth = 2;
    }
    Move *bestMove;
    double bestHeuristic;
    vector<Move *> availableMoves = board->possibleMoves(playerSide);

    //don't run if no available moves
    if(availableMoves.size() == 0){
        return NULL;
    }
    bestHeuristic = minimaxBranch(availableMoves[0], board, depth - 1, playerSide, true);
    bestMove = availableMoves[0];
    for(int i = 1; i < availableMoves.size(); i++){
        double curHeuristic = minimaxBranch(availableMoves[i], board, depth - 1, playerSide, true);
        if(curHeuristic > bestHeuristic){
            bestMove = availableMoves[i];
            bestHeuristic = curHeuristic;	    
        }
    }
    return bestMove;
}

/**
 * helper function for minimax
 *
 *@param move, move of parent node
 *@param lastBoard, board without parent node moved
 *@param depth, how many more plies to search, stops at 0
 *@param side, side that parent node's move should be
 *@param minimize, whether or not we are minimizing; false for maximizing
 */
double Player::minimaxBranch(Move *move, Board *lastBoard, int depth, Side side, bool minimize){
    Board *myBoard = lastBoard->copy(); //is this a memory leak?
    myBoard->doMove(move, side);
    if(depth == 0){
		return heuristic(myBoard);
    }

    double bestHeuristic;
    side = otherSide(side); //side is now the side of the possible moves
    vector<Move *> availableMoves = myBoard->possibleMoves(side);
    
    if(availableMoves.size() == 0){ //skip move, if depth > 2, then go back to turn player
        if(depth == 1){
            return heuristic(myBoard);
        }
        side = otherSide(side);
        availableMoves = myBoard->possibleMoves(side);
        if(availableMoves.size() == 0){ //neither side can move, so stop branch
            return heuristic(myBoard);
        }
        minimize = !minimize;
        depth--;
    }

    bestHeuristic = minimaxBranch(availableMoves[0], myBoard, depth - 1, side, !minimize);
    if(minimize){
		for(int i = 1; i < availableMoves.size(); i++){
			double curHeuristic = minimaxBranch(availableMoves[i], myBoard, depth - 1, side, !minimize);
			if(curHeuristic < bestHeuristic){
				bestHeuristic = curHeuristic;
			}
		}
    }
	else{ //maximize
		for(int i = 1; i < availableMoves.size(); i++){
			double curHeuristic = minimaxBranch(availableMoves[i], myBoard, depth - 1, side, !minimize);
			if(curHeuristic > bestHeuristic){
				bestHeuristic = curHeuristic;
			}
		}
    }
    delete myBoard;
    return bestHeuristic;
}

/*
 * Alpha-beta pruning with probcut
 */

Move *Player::probCut(int msLeft, double a, double b, double sigma, double T, int shallowDepth, int cutDepth, int depth){
	Move *bestMove; 
    double bestHeuristic;
    vector<Move *> availableMoves = board->possibleMoves(playerSide);
    //don't run if no available moves
    if(availableMoves.size() == 0){
        return NULL;
    }
    double alpha = -numeric_limits<double>::max();
    double beta = numeric_limits<double>::max();
    bestHeuristic = probCutBranch(availableMoves[0], board, 0, shallowDepth, cutDepth, depth - 1, a, b, sigma, T, alpha, beta, playerSide, true);
    bestMove = availableMoves[0];
    for(int i = 1; i <availableMoves.size(); i++){
        double curHeuristic = probCutBranch(availableMoves[i], board, 0, shallowDepth, cutDepth, depth - 1, a, b, sigma, T, alpha, beta, playerSide, true);
        if(curHeuristic > bestHeuristic){
            bestMove = availableMoves[i];
            bestHeuristic = curHeuristic;
        }
    }
    fprintf(stderr, "%f\n", bestHeuristic);
    return bestMove;
}

double Player::probCutBranch(Move *move, Board *lastBoard, double heuristicDP, int shallowDepth, int cutDepth,  int depth, double a, double b, double sigma, double T, double alpha, double beta, Side side, bool minimize){
	double hdp = heuristicDP;
    Board *myBoard = lastBoard->copy();
    myBoard->doMove(move, side);
    //return heuristic if terminal node or depth is done
    if(depth == 0 || myBoard->isDone()){ //WARNING: isDone is highly inefficient
		return heuristic(myBoard);
    }
	if(depth == shallowDepth){
		hdp = heuristic(myBoard);
	}
	if(depth == cutDepth){
		int bound = (int) ceil((T * sigma + beta - b) / a - 0.5);
		if(heuristicDP >= bound){
			return -numeric_limits<double>::max();
		}
		bound = (int) ceil((-T * sigma + alpha - b) / a - 0.5);
		if(heuristicDP <= bound){
			return -numeric_limits<double>::max();
		}
	}
    double bestHeuristic;
    side = otherSide(side);
    vector<Move *> availableMoves = myBoard->possibleMoves(side);

    if(availableMoves.size() == 0){ //skip move, if depth > 2, then go back to turn player
        if(depth == 1){
            return heuristic(myBoard);
        }
        side = otherSide(side); 
        availableMoves = myBoard->possibleMoves(side);
        if(availableMoves.size() == 0){
            return heuristic(myBoard);
        }
        minimize = !minimize;
        depth--;
    }

    bestHeuristic = alphabetaBranch(availableMoves[0], myBoard, depth - 1, alpha, beta, side, !minimize);
    if(!minimize){//maximizing
        alpha = max(alpha, bestHeuristic);
        for(unsigned i = 1; i < availableMoves.size(); i++){
            bestHeuristic = max(bestHeuristic, alphabetaBranch(availableMoves[i], myBoard, depth - 1, alpha, beta, side, !minimize));
            alpha = max(alpha, bestHeuristic);
            if(beta <= alpha){
                break;
            }
        }
        delete myBoard;
        availableMoves.clear();
        return bestHeuristic;
	
    }else{//minimizing
        beta = min(beta, bestHeuristic);
        for(unsigned i = 1; i < availableMoves.size(); i++){
            bestHeuristic = min(bestHeuristic, alphabetaBranch(availableMoves[i], myBoard, depth - 1, alpha, beta, side, !minimize));
            beta = min(beta, bestHeuristic);
            if(beta <= alpha){
                break;
            }
        }
        delete myBoard;
        availableMoves.clear();
        return bestHeuristic;
    }    
}

/** 
 * Simple implementation of alphabeta pruning
 * 
 * main method of alphabeta pruning, uses alphabetaBranch, which recurses itself
 *
 *@param msLeft milliseconds left for the entire game, implement time keeping later; maybe calculate
 * elsewhere, and have this value just be how much time for that specific move
 * @param depth number of plies to look, decremented by 1 for each level we go down
 */
Move *Player::alphabeta(int msLeft, int depth){
    Move *bestMove;
    double bestHeuristic;
    vector<Move *> availableMoves = board->possibleMoves(playerSide);
    
    //don't run if no available moves
    if(availableMoves.size() == 0){
        return NULL;
    }
    double alpha = -numeric_limits<double>::max();
    double beta = numeric_limits<double>::max();
    bestHeuristic = alphabetaBranch(availableMoves[0], board, depth - 1, alpha, beta, playerSide, true);
    bestMove = availableMoves[0];
    for(int i = 1; i <availableMoves.size(); i++){
        double curHeuristic = alphabetaBranch(availableMoves[i], board, depth - 1, alpha, beta, playerSide, true);
        if(curHeuristic > bestHeuristic){
            bestMove = availableMoves[i];
            bestHeuristic = curHeuristic;
        }
    }
    fprintf(stderr, "Evaluation: %f\n", bestHeuristic);
    return bestMove;
}

/**
 * helper function for alphabeta, called recursively
 *
 *@param move, move of parent node
 *@param lastBoard, board withHout parent node moved
 *@param depth, how many more plies to search, stops at 0
 *@param alpha, alpha value representing maximum score maximizing player assured of
 *@param beta, beta value representing minimum score minimizing player assured of
 *@param side, side that parent node's move should be for
 *@param minimzie, whether or not we are minimizing; false for maximizing
 */
double Player::alphabetaBranch(Move *move, Board *lastBoard, int depth, double alpha, double beta, Side side, bool minimize){
    Board *myBoard = lastBoard->copy();
    myBoard->doMove(move, side);

    //return heuristic if terminal node or depth is done
    if(depth == 0 || myBoard->isDone()){ //WARNING: isDone is highly inefficient
		return heuristic(myBoard);
    }
    double bestHeuristic;
    side = otherSide(side);
    vector<Move *> availableMoves = myBoard->possibleMoves(side);

    if(availableMoves.size() == 0){ //skip move, if depth > 2, then go back to turn player
        if(depth == 1){
            return heuristic(myBoard);
        }
        side = otherSide(side);
        availableMoves = myBoard->possibleMoves(side);
        if(availableMoves.size() == 0){
            return heuristic(myBoard);
        }
        minimize = !minimize;
        depth--;
    }

    bestHeuristic = alphabetaBranch(availableMoves[0], myBoard, depth - 1, alpha, beta, side, !minimize);
    if(!minimize){//maximizing
        alpha = max(alpha, bestHeuristic);
        for(unsigned i = 1; i < availableMoves.size(); i++){
            bestHeuristic = max(bestHeuristic, alphabetaBranch(availableMoves[i], myBoard, depth - 1, alpha, beta, side, !minimize));
            alpha = max(alpha, bestHeuristic);
            if(beta <= alpha){
                break;
            }
        }
        delete myBoard;
        availableMoves.clear();
        return bestHeuristic;
	
    }else{//minimizing
        beta = min(beta, bestHeuristic);
        for(unsigned i = 1; i < availableMoves.size(); i++){
            bestHeuristic = min(bestHeuristic, alphabetaBranch(availableMoves[i], myBoard, depth - 1, alpha, beta, side, !minimize));
            beta = min(beta, bestHeuristic);
            if(beta <= alpha){
                break;
            }
        }
        delete myBoard;
        availableMoves.clear();
        return bestHeuristic;
    }    
}

/**
 * makes a random move
 */
Move *Player::heuristicMove(){
    if(board->hasMoves(playerSide)){
	vector<Move *>availableMoves = board->possibleMoves(playerSide);
	Move *bestMove = availableMoves[0];
	Board *myBoard = board->copy();
	myBoard->doMove(availableMoves[0], playerSide);
	double bestHeuristic = heuristic(myBoard);
	for(unsigned i = 1; i < availableMoves.size(); i++){
	    myBoard = board->copy();
	    myBoard->doMove(availableMoves[i], playerSide);
	    if(heuristic(myBoard) > bestHeuristic){
		bestHeuristic= heuristic(myBoard);
		bestMove = availableMoves[i];
	    }
	}
	return bestMove;
    }
    return NULL;
}

/**
 * helper function, returns side that is not that side
 */
Side Player::otherSide(Side side){
    if(side == BLACK){
        return WHITE;
    }else{
        return BLACK;
    }
}

double Player::heuristic(Board *aBoard){
    double heuristicScore;
    if(testingMinimax){
        return aBoard->count(playerSide) - aBoard->count(oppSide);

    }else{
        if(aBoard->count(oppSide) == 0){
            delete aBoard;
            return 100000;
        }else if(aBoard->count(playerSide) == 0){
            delete aBoard;
            return -100000;
        }
        if(aBoard->count(playerSide) + aBoard->count(oppSide) == 64){
            
            heuristicScore = aBoard->count(playerSide) - aBoard->count(oppSide);
            delete aBoard;
            return heuristicScore;
        }

        //dynamic combination heuristic, implementation of Applications of Artificial Intelligence and Machine Learning in Othello by Jack Chen (DynamicPlayer)
        /*
        int totalMoves = aBoard->count(playerSide) + aBoard->count(oppSide);
        if(totalMoves < 15){
            double potMobWeights[] = {0, 0, 0, 0, 0, 0, -15, .5, -.5, 7, 6, 9, 5, 6, 5, 6};
            double movesWeights[] = {0, 0, 0, 0, 0, 0, 1, 9, 3, 3.4, 2.5, 2.5, 5, 7.5, 5};
            heuristicScore = 350 * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
            -200 * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) + \
            -80 * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) + \
            movesWeights[totalMoves] * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size()) + \
            potMobWeights[totalMoves] * (aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide));
        }else if(totalMoves < 50){
            if(totalMoves < 22){
                heuristicScore = 350 * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) - \
                80 * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) - \
                200 * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) - \
                8 * (aBoard->potentialMobility(oppSide) - aBoard->potentialMobility(playerSide));
                
            }else{
                heuristicScore = (-350/42 * (totalMoves - 22) + 350) * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
                (81/38 * (totalMoves - 22) - 80) * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) + \
                (200/38 * (totalMoves - 22) - 200) * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) + \
                (6/38 * (totalMoves - 22) - 8) * (aBoard->potentialMobility(oppSide) - aBoard->potentialMobility(playerSide));
                
            }
            heuristicScore += (20/35 * (totalMoves - 15) + 5) * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size());
            
        }else if (totalMoves < 62){
            heuristicScore = (-350/42 * (totalMoves - 22) + 350) * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
            (81/38 * (totalMoves - 22) - 80) * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) + \
            (200/38 * (totalMoves - 22) - 200) * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) + \
            (6/38 * (totalMoves - 22) - 8) * (aBoard->potentialMobility(oppSide) - aBoard->potentialMobility(playerSide)) + \
            (30/11 * (totalMoves - 50) + 250) * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size());
        }else{
            heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide);
        }
    
        delete aBoard;
        return heuristicScore;
        */
        
        //static-ish combination heuristic
        //3 game stages, 0, 25, 45
        /*
        int totalMoves = aBoard->count(playerSide) + aBoard->count(oppSide);
        if(totalMoves < 25){ //0-25
            heuristicScore = 350 * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
            -80 * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) + \
            -200 * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) + \
            2.5 * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size()) + \
            5 * (aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide));
        }else if(totalMoves < 45){
            heuristicScore = 250 * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
            -50 * (aBoard->cSquares(playerSide) - aBoard->cSquares(oppSide)) + \
            -100 * (aBoard->xSquares(playerSide) - aBoard->xSquares(oppSide)) + \
            15 * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size()) + \
            5 * (aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide));
        }else{
            heuristicScore = 50 * (aBoard->cornerScore(playerSide) - aBoard->cornerScore(oppSide)) + \
            30 * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size()) + \
            4 * (aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide)) + \
            10 * (aBoard->count(playerSide) - aBoard->count(oppSide));
        }
        
        */
        
        //static board heuristic + possibleMoves (AlphabetaPlayer1) <---- best player currently
        int playMoves = aBoard->count(playerSide);
        int oppMoves = aBoard->count(oppSide);
        if(playMoves + oppMoves < 25){
                heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + 2 * aBoard->possibleMoves(playerSide).size() - 2 * aBoard->possibleMoves(oppSide).size();
        }else if(playMoves + oppMoves < 50){
            heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size();
        }else{
                heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size();
        }
        delete aBoard;
        return heuristicScore;
        
        
        //static board heuristic + possibleMoves + mobility (MobilityPlayer)
        /*
         int playMoves = aBoard->count(playerSide);
         int oppMoves = aBoard->count(oppSide);
         if(playMoves + oppMoves < 20){
         heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + \
             2 * (aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size()) + \
             2 * (aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide));
         }else if(playMoves + oppMoves < 50){
         heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size() + \
             aBoard->potentialMobility(playerSide) - aBoard->potentialMobility(oppSide);
         }else{
             heuristicScore = aBoard->staticHeuristic(playerSide) - aBoard->staticHeuristic(oppSide) + aBoard->possibleMoves(playerSide).size() - aBoard->possibleMoves(oppSide).size();
         }
         delete aBoard;
         return heuristicScore;
         */
    }
}

