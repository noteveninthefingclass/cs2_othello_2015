#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdio>
#include "common.h"
#include "board.h"
using namespace std;



class Player {

private:
    Board *board;
    Side playerSide;
    Side oppSide;
    Algorithm algorithm;
    Move *minimax(int msLeft, int depth);
    double minimaxBranch(Move *move, Board *lastBoard, int depth, Side side, bool minimize);
	Move *probCut(int msLeft, double a, double b, double sigma, double T, int shallowDepth, int cutDepth, int depth);
	double probCutBranch(Move *move, Board *lastBoard, double heuristicDP, int cutDepth, int shallowDepth,  int depth, double a, double b, double sigma, double T, double alpha, double beta, Side side, bool minimize);
    Move *alphabeta(int msLeft, int depth);
    double alphabetaBranch(Move *move, Board *lastBoard, int depth, double alpha, double beta, Side side, bool minimize);
    Move *heuristicMove();
    double heuristic(Board *aBoard);
    Side otherSide(Side side);
    
public:
    Player(Side side);
    Player(Board *setupBoard, Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
};

#endif
